import React, { useState } from "react";
import { data_shoes } from "./Data_Shoes";
import Cart from "./Cart";
import ItemShoes from "./ItemShoes";

export default function BaiTapShoesHook() {
  const [cart, setCart] = useState([]);
  const handleAddToCart = (shoes) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => item.id === shoes.id);
    if (index !== -1) {
      cloneCart[index].soLuong++;
    } else {
      cloneCart.push({ ...shoes, soLuong: 1 });
    }
    setCart(cloneCart);
  };

  const handleQuantity = (shoes, num) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => item.id === shoes.id);
    if (index !== -1) {
      cloneCart[index].soLuong += num;
    }
    if (cloneCart[index].soLuong === 0) {
      cloneCart.splice(index, 1);
    }
    setCart(cloneCart);
  };

  const renderShoesList = () => {
    return data_shoes.map((shoes, index) => {
      return (
        <ItemShoes
          key={index}
          shoesData={shoes}
          handleAddToCart={handleAddToCart}
        />
      );
    });
  };
  return (
    <div className="container">
      <h1>Bai Tap Shoes Hook</h1>
      {cart.length > 0 && <Cart cart={cart} handleQuantity={handleQuantity} />}
      <div className="row">{renderShoesList()}</div>
    </div>
  );
}
