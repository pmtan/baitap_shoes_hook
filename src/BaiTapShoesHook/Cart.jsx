import React from "react";

export default function Cart(props) {
  const renderTbody = () => {
    return props.cart.map((shoes, index) => {
      return (
        <tr key={index}>
          <td>{shoes.name}</td>
          <td>
            <img src={shoes.image} alt="" style={{ width: "80px" }} />
          </td>
          <td>{shoes.price}</td>
          <td>
            <button
              onClick={() => props.handleQuantity(shoes, -1)}
              className="btn btn-danger mx-2"
            >
              -
            </button>
            {shoes.soLuong}
            <button
              onClick={() => props.handleQuantity(shoes, 1)}
              className="btn btn-success mx-2"
            >
              +
            </button>
          </td>
          <td>{shoes.price * shoes.soLuong}</td>
        </tr>
      );
    });
  };
  const calcTotal = () => {
    let total = props.cart.reduce(
      (sum, shoes) => sum + shoes.soLuong * shoes.price,
      0
    );
    return total;
  };
  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>Ten SP</th>
            <th>Hinh Anh</th>
            <th>Gia</th>
            <th>So Luong</th>
            <th>Thanh Tien</th>
          </tr>
        </thead>
        <tbody>{renderTbody()}</tbody>
        <tfoot>
          <tr>
            <td colSpan={4} className="text-right font-weight-bold">
              Tong Cong
            </td>
            <td className="font-weight-bold">{calcTotal()}</td>
          </tr>
        </tfoot>
      </table>
    </div>
  );
}
