import React from "react";

export default function ItemShoes(props) {
  let shoes = props.shoesData;
  return (
    <div className="col-3 mt-3">
      <div className="card h-100 mt-3">
        <img
          className="card-img-top"
          src={shoes.image}
          alt=""
          style={{ weight: "100%" }}
        />
        <div className="card-body d-flex flex-column justify-content-between">
          <h5 className="card-title">{shoes.name}</h5>
          <p className="card-text">
            {shoes.description.length < 70
              ? shoes.description
              : shoes.description.substring(0, 70) + "..."}
          </p>
          <button
            onClick={() => props.handleAddToCart(shoes)}
            className="btn btn-success"
          >
            Add to Cart
          </button>
        </div>
      </div>
    </div>
  );
}
