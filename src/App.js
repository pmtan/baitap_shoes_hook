import "./App.css";
import BaiTapShoesHook from "./BaiTapShoesHook/BaiTapShoesHook";

function App() {
  return (
    <div className="App">
      <BaiTapShoesHook />
    </div>
  );
}

export default App;
